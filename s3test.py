#!venv/bin/python
import re
import boto3
import requests
from botocore.client import Config

def presignGetRequest(bucket_name, key_name):
    # Get the service client with sigv4 configured
    s3 = boto3.client('s3', config=Config(signature_version='s3v4'))
    # Generate the URL to get 'key-name' from 'bucket-name'
    url = s3.generate_presigned_url(
        ClientMethod='get_object',
        Params={
            'Bucket': bucket_name,
            'Key': key_name
        }
    )
    return url

def presignPostRequest(bucket_name, key_name):
    # Get the service client with sigv4 configured
    s3 = boto3.client('s3', config=Config(signature_version='s3v4'))
    # Generate the URL to get 'key-name' from 'bucket-name'
    url = s3.generate_presigned_url(
        'put_object',
        Params={
            'Bucket': bucket_name,
            'Key': key_name
        }
    )
    return url

def getS3objectTest(elb, bucket_name, s3object_name):
    print '------------------------------------------------------------------'
    print 'Get S3 Object'
    print '------------------------------------------------------------------'
    print 'elb     : {}'.format(elb)
    print 'bucket  : {}'.format(bucket_name)
    print 's3object: {}'.format(s3object_name)
    # generate the url
    url = presignGetRequest(bucket_name, s3object_name)
    print 'original url : {}'.format(url)
    # swap out the domain with the elb hostname
    url = url.replace('s3.amazonaws.com', elb)
    print 'rewritten url: {}'.format(url)
    resp = requests.get(url, verify=False)
    print '------------ response ------------'
    print 'status_code: {}'.format(resp.status_code)
    print 'response: {}'.format(resp.text)

def putS3objectTest(elb, bucket_name, s3object_name, files):
    print '------------------------------------------------------------------'
    print 'Put S3 Object'
    print '------------------------------------------------------------------'
    print 'elb     : {}'.format(elb)
    print 'bucket  : {}'.format(bucket_name)
    print 's3object: {}'.format(s3object_name)
    # generate the url
    url = presignPostRequest(bucket_name, s3object_name)
    print 'original url : {}'.format(url)
    # swap out the domain with the elb hostname
    url = url.replace('s3.amazonaws.com', elb)
    print 'rewritten url: {}'.format(url)
    resp = requests.put(url, verify=False, files=files)
    print '------------ response ------------'
    print 'status_code: {}'.format(resp.status_code)
    print 'response: {}'.format(resp.text)

# define the test parameters
raw_elb = 'proxy-test-elb-660155606.us-east-1.elb.amazonaws.com'
bucket  = 'dfs-lab16-test1'
s3object  = 'message.txt'
files = {"file": "hello world"}

# upload the object then read the object
putS3objectTest(raw_elb, bucket, s3object, files)
getS3objectTest(raw_elb, bucket, s3object)
