resource "aws_instance" "proxy" {
  count = 1
  ami           = "ami-b63769a1"
  instance_type = "t2.micro"
  iam_instance_profile   = "${aws_iam_instance_profile.test_profile.name}"
  subnet_id     = "${data.aws_subnet.default.id}"
  key_name      = "${var.tags["Name"]}"
  tags = "${var.tags}"

  provisioner "remote-exec" {
    script  = "chef.sh"
    connection {
      user = "ec2-user"
      private_key = "${file("./LAB16.pem")}"
      host = "${self.public_ip}"
    }
  }
  # add SSH security group (for troubleshooting/testing and for remote-exec to function)
  vpc_security_group_ids = ["sg-fd51909b", "${aws_security_group.elb_to_proxy.id}"]
}
