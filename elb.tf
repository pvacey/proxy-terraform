# Create a new load balancer
resource "aws_elb" "proxy-test" {
  name               = "proxy-test-elb"
  subnets = ["${aws_instance.proxy.*.subnet_id}"]
  security_groups = ["${aws_security_group.any_to_elb.id}"]

  listener {
    instance_port     = 8443
    instance_protocol = "tcp"
    lb_port           = 443
    lb_protocol       = "tcp"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 3
    timeout             = 3
    interval            = 5
    target              = "HTTPS:8443/elb-status"
  }
  instances = ["${aws_instance.proxy.*.id}"]
  tags = "${var.tags}"
}
