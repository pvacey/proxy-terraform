#!/bin/bash
sudo bash <<"EOF"
curl -L https://omnitruck.chef.io/install.sh | sudo bash
yum install git -y
git clone https://bitbucket.org/pvacey/calamari.git
cd calamari
chef-client -z -o nginx
EOF
