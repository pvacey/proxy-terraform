provider "aws" {
  region     = "us-east-1"
}

# default lab VPC here
data "aws_vpc" "default" {
    id = "vpc-e6032a83"
}

# the default lab subnet
data "aws_subnet" "default"{
    id = "subnet-49f27362"
}

# tags for lab
variable "tags" {
    type = "map"
    default {
        Name = "LAB16"
        Costcenter = "458001"
        Purpose = "aws broker proxy"
        Usage = "aws broker proxy"
        Disposable = "true"
    }
}
