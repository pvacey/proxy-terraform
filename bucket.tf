resource "aws_s3_bucket" "b" {
  bucket = "dfs-lab16-nginx-private"
  acl    = "private"
}

resource "aws_s3_bucket_object" "o" {
  bucket = "${aws_s3_bucket.b.bucket}"
  key    = "server.key"
  content   = <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIICXQIBAAKBgQC5r/u3WvCb3kYVZDYG97yqR0NUbcnPzWfXcuuzV5iEP+osFpyb
/MPDYS8N1Mif4xtDmQ2i12GU9Ak44iVBIMYqvDgYorLZ7TloAmqcIDURGgsDfxTX
q1feYx68xrLdG4VSyO5PJjDVr23IHzElGtWakYglWcQiwuvS8+2VWbzjkwIDAQAB
AoGBAKev9XGv/nnEUSYgSWohb4+Wq4cjXoz6wjwe3BwpjuJfleoawXNc4K0gpo2n
C0bYoVZ6cYYNW2JYv7tO/rXCEzIaXAYiCJ5nGAJ89wsdCqFrl8y4JbqtbVBohGHK
w8ZFrSB6XUjOl21rthrCdqmrxXvBV0MGmXS0aThVjn1gRnkBAkEA5Vq7JGsRFgAO
xsjHrY+4hRsUs0QXXH6Ih33BGDkJBLXbvqRryTOQFgdSdXoUCpvoimn0OPDZ0Z8+
0fT9INI+uQJBAM9Ci4ZiFObEn2NirWWcJeUKQ9pWqACcBswbBXGMgpctJVesOAW3
mMYGKdfe4L7o5bJTQWa9ku9ey12q//hl7qsCQCRnLEDxhXq6c531jEvt/dmSnMx/
SD30D3OB8D4J/3Zx7vjpW/pJZts4OkxPHhM4/8XIX20SPWBaKDxm6C5ncHECQAQ0
w4QcpXPxdZKcxAwV2ScGyE28CHjKsdCek8O0KEpNHj7jPdfP3AfYxCyfV0xIibDO
w7sECeFVuSbTe5Jc7zUCQQClj8xV1OQvr5CEP4CbjkWTwaFARQW21GTG95Il1yTx
EmDhGWkoxPyT44AtdrgACNqqSVAB7BisZExJF2WXSD3H
-----END RSA PRIVATE KEY-----
EOF
}
