#############
# elb rules #
#############

resource "aws_security_group" "any_to_elb" {
  name         = "any_to_elb"
  description  = "allow source to hit load balancer port"
  vpc_id       = "${data.aws_vpc.default.id}"
  tags         = "${var.tags}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "elb_http_i" {
    security_group_id = "${aws_security_group.any_to_elb.id}"
    type              = "ingress"
    from_port         = 80
    to_port           = 80
    protocol          = "tcp"
    cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "elb_https" {
    security_group_id = "${aws_security_group.any_to_elb.id}"
    type              = "ingress"
    from_port         = 443
    to_port           = 443
    protocol          = "tcp"
    cidr_blocks       = ["0.0.0.0/0"]
}

########################
# nginx instance rules #
########################

resource "aws_security_group" "elb_to_proxy" {
  name        = "elb_to_proxy"
  description = "allow inbound proxy port traffic from ELB only"
  vpc_id      = "${data.aws_vpc.default.id}"
  tags        = "${var.tags}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "proxy_http"{
    security_group_id        = "${aws_security_group.elb_to_proxy.id}"
    type                     = "ingress"
    from_port                = 8000
    to_port                  = 8000
    protocol                 = "tcp"
    source_security_group_id = "${aws_security_group.any_to_elb.id}"
}

resource "aws_security_group_rule" "proxy_https"{
    security_group_id        = "${aws_security_group.elb_to_proxy.id}"
    type                     = "ingress"
    from_port                = 8443
    to_port                  = 8443
    protocol                 = "tcp"
    source_security_group_id = "${aws_security_group.any_to_elb.id}"
}
